import matplotlib.pyplot as plt
import pandas as pd
from asteval import Interpreter
from matplotlib.pyplot import figure
from collections import Counter


class Data_Visualizer :

    def __init__(self,file_p_1,file_p_2):
        self.data = pd.read_csv("./data/cleaned_data.csv")
        self.param_bar = file_p_1
        self.param_scatter = file_p_2

    
    def ploting_bar(self,name,col_x,col_y,data):


        params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'medium',
         'axes.titlesize':'medium',
         'xtick.labelsize':'xx-small',
         'ytick.labelsize':'small'}
        plt.rcParams.update(params)
        figure(num=None, figsize=(20, 20), dpi=200, facecolor='w', edgecolor='k')
        plt.tight_layout()
        x = data[col_x]
        y = data[col_y]
        plt.bar(x,y)
        plt.rc('xtick')
        plt.title(name)
        plt.ylabel("euros")
        plt.xlabel(col_x)
        plt.xticks(rotation=90)
        plt.savefig("./figures/"+name)
        
        


    def freq_off_zone(self, type_O, zone):


        params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'medium',
         'axes.titlesize':'medium',
         'xtick.labelsize':'xx-small',
         'ytick.labelsize':'small'}
        plt.rcParams.update(params)
        figure(num=None, figsize=(20, 20), dpi=200, facecolor='w', edgecolor='k')
        plt.tight_layout()

        d = self.data[self.data["typeOffre"] == type_O]
        dic = Counter(d[zone])
        p  = pd.DataFrame(list(dic.items()),columns = ["key","value"]) 
        
        x = p.key
        Y = p.value
       
        plt.scatter(x,Y, label="nombre d offres",color="g")
        
        plt.xlabel(zone)
        plt.xticks(rotation=90)
        if(zone == "ville"):
            plt.tick_params(axis='x', which='major', labelsize=1)

        plt.ylabel("offres "+ type_O+" "+zone)
        plt.title(" nbr d'offre  "+type_O+"  par "+ zone)
        
        plt.savefig("./figures/freq/"+"Offre_"+type_O+"_par_"+ zone)
 

    def scatterer(self):
        parameter = pd.read_csv(self.param_scatter, sep=" ", header= None)
        parameter.columns = ["type_O","zone"]
        def func(i):
            self.freq_off_zone(
                parameter.at[i,'type_O'],
                parameter.at[i,'zone']
            )
        
        indexes = range(len(parameter))
        list(map(func,indexes))
            
    
    def bar_makfer(self):
        parameter = pd.read_csv(self.param_bar, sep=" ", header= None)
        parameter.columns = ["name","col_x","col_y","data"]
        def func(i):
            data = pd.read_csv("./results/"+parameter.at[i,'data'])
            self.ploting_bar(
                parameter.at[i,'name'],
                parameter.at[i,'col_x'],
                parameter.at[i,'col_y'],
                data
            )
            
        indexes = range(len(parameter))
        list(map(func,indexes))

            
        

    def exec(self,g_type):

        if(g_type == "scatter"):
            self.scatterer()
            
        elif(g_type == "bar"):
             self.bar_makfer()
