import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import linear_model as lm
from sklearn.model_selection import train_test_split


class Linear_Model:

   def __init__(self, path):
        df = pd.read_csv(path, low_memory=False)
        self.data = df

   def _model_training(self,x,y):
        reg = lm.LinearRegression()
        reg.fit(x,y)
        return reg   

   def _model_testing(self,model,x,y):
        res = model.predict(x)
        # mesure de l'erreure MSE
        MSE =np.mean((res - y)**2)

        return [res,MSE]

   def _input_prep(self,input):
        x = pd.read_csv(input)
        D_order = x[["region","departement","ville","typeBien","taille","superficie"]]
        df = D_order.dropna()
        df = self._normalizer(self._normalizer(self._normalizer(self._normalizer(df,"typeBien"),"ville"),"departement"),"region")
        x = self._x_prep(df)
        return x

   def _x_prep(self,data):
        X = data[["region","departement","ville","typeBien","taille","superficie"]]
        return X

   def _y_prep(self,data):
        Y = data["prix"]
        return Y

   def _normalizer(self,df,zone):

        dic  = df[zone].unique()
        i_dic = dict(zip(dic, range(len(dic))))
        df.applymap(lambda s: i_dic.get(s) if s in i_dic else s)
        df[zone] = [i_dic[item] for item in df[zone]]

        return df
   
   def _data_prep(self,data, type_O):
        D_order = data [["typeOffre","region","departement","ville","typeBien","taille","superficie","prix"]]
        D_order = D_order.dropna()
        df = D_order.loc[D_order["typeOffre"] == type_O ]
        df = self._normalizer(self._normalizer(self._normalizer(self._normalizer(df,"typeBien"),"ville"),"departement"),"region")
        return df
        
   def _model_generator(self,type_O):
        # data retrival 
        d = self._data_prep(self.data,type_O)
        X = self._x_prep(d)
        Y = self._y_prep(d)

        # répartition de données entre test et entrainement
        x_train,x_test,y_train,y_test = train_test_split(X,Y,test_size=0.2,random_state = 40)

        # entrainement
        model = self._model_training(x_train,y_train)
        test = self._model_testing(model,x_test,y_test)
        return [model,test]
   
   def prediction (self,type_o,input):
       m = self._model_generator(type_o)[0]
       x = self._input_prep(input)

       return m.predict(x)





