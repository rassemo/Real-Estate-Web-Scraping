import pandas as pd
import bs4 as bs
from selenium import webdriver
import os
import json
import csv
import logging

class Web_Scraper:
    
    def __init__(self,driver,url_config_file,geo_data_file,population,scraping_result_file):
        self.__driver = driver
        self.__url_config_file = url_config_file
        self.__geo_data_file = geo_data_file
        self.__population = population
        self.__scraping_result_file = scraping_result_file
        df = pd.read_csv(self.__geo_data_file,header=0)
        self.__gdata = df[df['population'] >= self.__population]
    
    def start(self):
        with open(self.__url_config_file, "r") as f:
            urls = json.load(f)
            list(map(self.process_url,urls))

    def process_url(self,url_in):
        result = []
        extractor = self.switch_extractor(url_in['func_num'])
        list(map(self.scrap_data(url_in,extractor,result),self.__gdata[url_in['research_param']].values))
        self.write_csv(result,['typeOffre','region','departement','ville','typeBien','prix','taille','superficie'])     

    def scrap_data(self,url_in,extractor,result):
        def func(url_param):
            url = url_in['url_base'] + str(url_param)
            self.__driver.get(url)
            logging.info('GET '+url)
            
            soup = bs.BeautifulSoup(self.__driver.page_source,'html.parser')
            divs = soup.select(url_in['css_selector'])

            list(map(self.apply_extractor(extractor,url_in,url_param,result),divs))

        return func

    def apply_extractor(self,extractor,url_in,url_param,result):
        def func(div):
            try:
                unit = extractor(self,self.__gdata,url_in['research_param'],url_in['offer'],url_param,div)
                result.append(unit)
            except IndexError:
                pass # ignore input having an unexpected format
        
        return func

    def switch_extractor(self,func_num):
        switcher = {
            1:self.extract_seloger,
            2:self.extract_laforet
        }
        return switcher.get(func_num, lambda: "Invalid agument func_num")

    def write_csv(self,dict_list,fields):
        with open(self.__scraping_result_file,'a') as out_f:
            writer = csv.DictWriter(out_f, fieldnames=fields)
            if(os.stat(self.__scraping_result_file).st_size == 0):
                writer.writeheader()
            list(map(writer.writerow,dict_list))       
    
    # www.seloger.com
    @staticmethod
    def extract_seloger(self,gdata,research_param,offer_type,value,div):
        unit = {
            "typeOffre":offer_type,
            "region":gdata[gdata[research_param] == value]['region'].values[0],
            "departement":gdata[gdata[research_param] == value]['department'].values[0],
            "ville": gdata[gdata[research_param] == value]['city'].values[0],
            "typeBien":div.find('div',{"class":"Card__LabelGap-sc-7insep-5 ContentZone__Title-wghbmy-6 hXERKq"}).text,
            "prix":  div.find('div',{"class":"Price__Label-sc-1g9fitq-1 kYWVBR"}).text.replace("€","").strip(),
            "taille": div.select('ul li')[0].text.replace("p","").strip(),
            "superficie":div.select('ul li')[2].text.replace("m²","").strip()            
        }
        return unit

    # www.laforet.com
    @staticmethod
    def extract_laforet(self,gdata,research_param,offer_type,value,div):
        unit = {
            "typeOffre":offer_type,
            "region":gdata[gdata[research_param] == value]['region'].values[0],
            "departement":gdata[gdata[research_param] == value]['department'].values[0],             
            "ville": gdata[gdata[research_param] == value]['city'].values[0],
            "typeBien":div.find('h4',{"class":"property-card__title"}).text.split(" ")[0],
            "prix":  div.find('span',{"class":"property-card__price"}).text.replace("€","").strip(),
            "taille":div.select('div[class="property-card__infos"] span')[1].text.replace(" pièce(s)","").strip(),
            "superficie":div.select('div[class="property-card__infos"] span')[0].text.replace("m²","").strip()
        }
        return unit
    



