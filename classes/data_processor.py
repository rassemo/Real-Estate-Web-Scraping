import pandas as pd
import math
import matplotlib as mb
import numpy as np
from asteval import Interpreter
import logging


class Data_Processor:

  def __init__(self, data_path, param_path):
    self.data = pd.read_csv(data_path)
    self.param = pd.read_csv(param_path, sep=" ", header= None)
    self.param.columns = ["zone","type_offre","col_name","depending","op_type","file_name"]
    
  # function that returns the fraction price by a given variable
  # zone : etandue de la zone considérée pour le calcule
  # type_offre : either achat / location
  # col_name : name of the result column that hold the product of the finction
  # depending : the given variable used to process the fraction
  # op_type : TRUE => mean(), FALSE => median()

  def price_per_variable(self, zone, type_offre, col_name, depending, op_type):
    # ajout de la colonne prix au metre carré
    aeval = Interpreter()
    d = self.data
    d[col_name] = d['prix'] / d[depending]
    t = d.loc[d["typeOffre"] == type_offre]
    if(op_type):
      S = t.groupby(aeval(zone), as_index=False)[col_name].mean()
    else:
      S = t.groupby(aeval(zone), as_index=False)[col_name].median()

    return S

  def exec(self):
    def func(i):
      result = self.price_per_variable( 
        self.param.at[i,'zone'],
        self.param.at[i,'type_offre'],
        self.param.at[i,'col_name'],
        self.param.at[i,'depending'],
        self.param.at[i,'op_type']
      )
      result.to_csv("./results/"+  self.param.at[i,'file_name'])
    
    indexes = range(len(self.param))
    list(map(func,indexes))