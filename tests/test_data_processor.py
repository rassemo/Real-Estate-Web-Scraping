import sys
sys.path.append("..")
import unittest
import pandas as pd
import numpy as np 
from classes import Data_Processor

class Test_Data_Processor(unittest.TestCase): 

    def __init__(self, *args, **kwargs):
        super(Test_Data_Processor, self).__init__(*args, **kwargs)
        self.processor = Data_Processor("./test_data/test_processor_data.csv","./test_data/test_processor_param.txt")  
    
    def test_data_processing(self):
        D = self.processor.data 
        P = self.processor.param
        
        result = self.processor.price_per_variable(
            P.at[0,'zone'],
            P.at[0,'type_offre'],
            P.at[0,'col_name'],
            P.at[0,'depending'],
            P.at[0,'op_type']
        )

        rows = [
            ["VAL-D'OISE",12.0],
            ["VAL-DE-MARNE",26.0]
        ]

        expected = pd.DataFrame(rows,columns=['departement','prix/m**2'])

        self.assertTrue(result.equals(expected))

        result = self.processor.price_per_variable(
            P.at[1,'zone'],
            P.at[1,'type_offre'],
            P.at[1,'col_name'],
            P.at[1,'depending'],
            P.at[1,'op_type']
        )

        rows = [
            ["VAL-D'OISE",100.0],
            ["VAL-DE-MARNE",250.0]
        ]
        
        expected = pd.DataFrame(rows,columns=['departement','prix/taille'])

        self.assertTrue(result.equals(expected))

        self.processor.exec()


if __name__ == '__main__':
    unittest.main()